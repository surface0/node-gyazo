# Gyazo Server on Node.js

## Build

```
% npm i && npm run build
```

## Run Server

```
% npm start
```

## Configure

Put the `.env` file.  

```
LISTEN_PORT=3000
VIEW_URL_HOST="http://127.0.0.1:3000"
VIEW_URL_PATH="/view"
UPLOAD_URL_PATH="/upload"
UPLOAD_FILE_PATH="/uploads"
```
